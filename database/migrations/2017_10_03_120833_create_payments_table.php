<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('plan_id')->unsigned();
            $table->integer('client_id')->unsigned();
            $table->double('amount');
            $table->string('payment_method', 20);
            $table->dateTime('start_date');
            $table->dateTime('expiry_date');
            $table->foreign('plan_id')
                ->references('id')->on('plans')
                ->onUpdate('cascade')->onDelete('no action');
            $table->foreign('client_id')
                ->references('id')->on('clients')
                ->onUpdate('cascade')->onDelete('no action');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
