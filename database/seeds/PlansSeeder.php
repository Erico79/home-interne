<?php

use Illuminate\Database\Seeder;
use App\Plan;

class PlansSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Plan::create([
            'plan_name' => '1 Day (24 hrs)',
            'price' => 40,
            'duration' => '24 hrs'
        ]);

        Plan::create([
            'plan_name' => '1 Week (7 Days)',
            'price' => 240,
            'duration' => '7 Days'
        ]);

        Plan::create([
            'plan_name' => '2 Weeks (14 Days)',
            'price' => 440,
            'duration' => '2 Weeks'
        ]);

        Plan::create([
            'plan_name' => '3 Weeks (21 Days)',
            'price' => 700,
            'duration' => '3 Weeks'
        ]);

        Plan::create([
            'plan_name' => '1 Month',
            'price' => 1000,
            'duration' => '1 Month'
        ]);
    }
}
