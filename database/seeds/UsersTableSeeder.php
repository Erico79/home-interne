<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::Create([
            'name' => 'Eric Murimi Njue',
            'email' => 'emurinyo@gmail.com',
            'password' => bcrypt('erick12!@')
        ]);
    }
}
