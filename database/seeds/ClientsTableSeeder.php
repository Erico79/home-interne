<?php

use Illuminate\Database\Seeder;
use App\Client;

class ClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Client::create(['client_name' => 'John Mtoto Mnyika']);
        Client::create(['client_name' => 'Joel Wathimba Beatrice']);
        Client::create(['client_name' => 'Esther Naitore']);
        Client::create(['client_name' => 'Oliver Kairi Mwirigi']);
        Client::create(['client_name' => 'Makena']);
        Client::create(['client_name' => 'Murithi']);
        Client::create(['client_name' => 'Joseph Gatura Mwangi']);
        Client::create(['client_name' => 'Delick Souja']);
        Client::create(['client_name' => 'Collins Murithi']);
        Client::create(['client_name' => 'Francis Njeru']);
        Client::create(['client_name' => 'Geoffrey Mwenda']);
        Client::create(['client_name' => 'Laurent Righa Lenjo']);
    }
}
