<?php

namespace App\Providers;

use App\Payment;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        view()->composer(['layouts.app'], function ($view) {
            $total_sales = Payment::totalSales();
            $view->with('total_sales', $total_sales);
        });
    }
}
