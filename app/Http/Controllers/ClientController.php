<?php

namespace App\Http\Controllers;

use App\Client;
use App\Payment;
use App\Plan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('clients.index')->with('clients', Client::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('clients.register');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'unique:clients',
            'phone_no' => 'unique:clients'
        ]);

        Client::create([
            'client_name' => $request->name,
            'email' => $request->email,
            'phone_no' => $request->phone_no
        ]);

        return redirect('client/clients');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function showPlans($id) {
        $plans = Plan::all();
        return view('clients.add-plan')
            ->with('plans', $plans)
            ->with('client_id', $id);
    }

    public function attachPlan(Request $request) {
        $this->validate($request, [
            'plan' => 'required|numeric',
            'payment_method' => 'required',
            'client_id' => 'required'
        ]);

        // attach client to plan
        $plan = Plan::find($request->plan);

        $start_date = date('Y-m-d H:i:s');
        $expiry_date = $this->determineExpiryDate($start_date, $plan);

        $client = Client::find($request->client_id);
        $client->plans()->attach($request->plan);

        // record payment
        Payment::create([
            'plan_id' => $request->plan,
            'client_id' => $request->client_id,
            'amount' => $plan->price,
            'payment_method' => $request->payment_method,
            'start_date' => $start_date,
            'expiry_date' => $expiry_date
        ]);

        $request->session()->flash('success', $client->name . ' has been attached to the <b>' . $plan->plan_name . '</b>');
        return redirect('client/clients');
    }

    private function determineExpiryDate($start_date, $plan) {
        return date('Y-m-d H:i:s', strtotime('+' . $plan->duration, strtotime($start_date)));
    }

    public function paymentHistory($id) {
        $payments = Payment::where('client_id', $id);
        $total = $payments->sum('amount');

        return view('clients.payments', [
            'payments' => $payments->get(),
            'total' => $total,
            'client' => Client::find($id)
        ]);
    }
}
