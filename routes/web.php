<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::group(['middleware' => 'auth', 'prefix' => 'client'], function(){
    Route::get('clients', 'ClientController@index');
    Route::get('registration', 'ClientController@create');
    Route::post('registration', 'ClientController@store');

    Route::get('plan/{id}', 'ClientController@index');
    Route::get('plan/{id}', 'ClientController@showPlans');
    Route::post('attach-plan', 'ClientController@attachPlan');

    Route::get('payments/{id}', 'ClientController@paymentHistory');
});
