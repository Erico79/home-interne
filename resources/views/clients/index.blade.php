@extends('layouts.app')
@section('title', 'All Registered Clients')

@push('js')
    <script>
        $(document).ready(function() {
            var table = $('#clients-list').DataTable( {
                responsive: true
            } );

            new $.fn.dataTable.FixedHeader( table );
        } );
    </script>
@endpush

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Clients List</div>

                    <div class="panel-body">
                        <table id="clients-list" class="table table-striped table-bordered nowrap" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Client#</th>
                                    <th>Name</th>
                                    <th>Phone</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>

                            <tbody>
                                @if(count($clients))
                                    @foreach($clients as $client)
                                        <tr>
                                            <td>{{ $client->id }}</td>
                                            <td>{{ $client->client_name }}</td>
                                            <td>{{ $client->phone_no }}</td>
                                            <td>
                                                <a href="{{ url('client/plan/' . $client->id) }}" class="btn btn-xs btn-primary">
                                                    <i class="fa fa-paperclip"></i> Add Plan</a>
                                                <a href="{{ url('client/payments/' . $client->id) }}" class="btn btn-xs btn-success">
                                                    <i class="fa fa-list"></i> Payment History</a>
                                                <button class="btn btn-xs btn-warning edit-btn" client-id="{{ $client->id }}">
                                                    <i class="fa fa-edit"></i> Edit</button>
                                                <button class="btn btn-xs btn-danger edit-btn" client-id="{{ $client->id }}">
                                                    <i class="fa fa-edit"></i> Delete</button>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection