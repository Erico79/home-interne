@extends('layouts.app')
@section('title', 'Payment History')

@push('js')
    <script>
        $(document).ready(function() {
            var table = $('#payments-history').DataTable( {
                responsive: true
            } );

            new $.fn.dataTable.FixedHeader( table );
        } );
    </script>
@endpush

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-money"></i> {{ $client->client_name }}
                        <span class="pull-right">Total: {{ number_format($total, 2) }}</span>
                    </div>

                    <div class="panel-body">
                        <table id="payments-history" class="table table-striped table-bordered nowrap" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>Payment#</th>
                                <th>Plan</th>
                                <th>Amount</th>
                                <th>Start Time</th>
                                <th>Expiry Time</th>
                            </tr>
                            </thead>

                            <tbody>
                            @if(count($payments))
                                @foreach($payments as $payment)
                                    <tr>
                                        <td>{{ $payment->id }}</td>
                                        <td>{{ \App\Plan::find($payment->plan_id)->plan_name }}</td>
                                        <td>{{ number_format($payment->amount,2) }}</td>
                                        <td>{{ $payment->start_date }}</td>
                                        <td>{{ $payment->expiry_date }}</td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection