@extends('layouts.app')
@section('title', 'Client Registration')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Attach Client to Plan</div>

                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ url('client/attach-plan') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('plan') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Choose Internet Plan</label>

                                <div class="col-md-6">
                                    @if(count($plans))
                                        @foreach($plans as $plan)
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="plan" value="{{ $plan->id }}">
                                                {!! $plan->plan_name . ' - (<b>Ksh. ' . number_format($plan->price, 2) . '</b>)' !!}
                                            </label>
                                        </div>
                                        @endforeach
                                    @endif

                                    @if ($errors->has('plan'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('plan') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('payment_method') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Payment Method</label>

                                <div class="col-md-6">
                                    <select name="payment_method" class="form-control">
                                        <option value="Cash">Cash</option>
                                        <option value="Mpesa">Mpesa</option>
                                    </select>

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <input type="hidden" name="client_id" value="{{ $client_id }}"/>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Buy
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
